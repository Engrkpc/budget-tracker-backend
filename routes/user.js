const express= require('express');
const router = express.Router();
const auth = require('../auth')
const UserController = require('../controllers/user')

//if email-exist for testing
router.post('/email-exists', (req, res) =>{
    UserController.emailExists(req.body).then(result => res.send(result))
})

//register working
router.post('/', (req, res) =>{
    UserController.register(req.body).then(result => res.send(result))
})

//login working
router.post('/login', (req,res) =>{
    UserController.login(req.body).then(result => res.send(result))
})

//adding categories
router.post('/categories', (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryName: req.body.categoryName,
        categoryType: req.body.categoryType
    };
    UserController.createCategory(params).then((result) => res.send(result));
});

//adding records

router.post('/records', (req, res) =>{
    const params ={
        userId: auth.decode(req.headers.authorization).id,
        categoryType: req.body.categoryType,
        categoryName: req.body.categoryName,
        amount: req.body.amount,
        description: req.body.description
    };

    UserController.addRecord(params).then((result) => res.send(result))
});


//user details

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})


//google routes
router.post('/verify-google-id-token', async (req, res) => {
    res.send( await UserController.verifyGoogleTokenId(req.body.tokenId))
})


// // for soft delete
// router.delete('/:categoryId', auth.verify, (req, res) => {
// 	const categoryId = req.params.categoryId
//     UserController.archive({ categoryId }).then(result => res.send(result))
// })



module.exports = router