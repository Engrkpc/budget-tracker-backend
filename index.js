const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors =require('cors');

// app.use(cors());
// for vercel hosting
app.use(cors());
app.options("*", cors());

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://admin:admin@wdc028-course-booking.ux8nl.mongodb.net/budget_tracker?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
    useFindAndModify: false

})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoutes = require('./routes/user')

app.use('/api/users', userRoutes)


app.listen(process.env.PORT || 4000, () =>{
    console.log(`API is online on PORT ${process.env.PORT || 4000}`)
})