
const { OAuth2Client } = require('google-auth-library');

const User = require('../models/user')
const bcrypt = require('bcryptjs');
const auth = require('../auth')
// const clientId ='10444938299-43406hruphuohmc78cqr4v6mlv01t4op.apps.googleusercontent.com'
const clientId ='551065054139-r5r2kgdkiudpf3ok11ecf91teopl50tq.apps.googleusercontent.com'


//if email exists
module.exports.emailExists = (params) =>{
    return User.find({email:params.email}).then(result =>{
        return result.length > 0 ? true : false
    })
}


//register controller 
//params are the details input by the user inside the object { }
//working
module.exports.register = (params) =>{

            let user = new User({
                firstName: params.firstName,
                lastName: params.lastName,
                email: params.email,
                mobileNo: params.mobileNo,
                password: bcrypt.hashSync(params.password,10),
                loginType: 'email'
            })

            return user.save().then((user,err) =>{
                return (err) ? false : true
            })

}

//login testing returning access token 
//working
module.exports.login =(params) =>{
    return User.findOne({email:params.email})
    .then(user =>{
        if (user === null){
            return { error: 'does-not-exist'}
        }
        
        if (user.loginType !== 'email'){
			return { error: 'login-type-error'}
		}

    const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject()) }
		}else{ 
			return { error: 'incorrect-password'}
		}
    })
}



//categories
//Working
module.exports.createCategory = (params) => {
    console.log(params)
    return User.findById(params.userId).then((user) =>{
        user.categories.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType
        });

        return user.save().then((user, err) =>{
            return err ? false : true;
        });
    });
};

//addRecord
module.exports.addRecord =(params) =>{
    console.log(params)
    return User.findById(params.userId).then((user) =>{

        user.records.push({
            categoryName: params.categoryName,
            categoryType: params.categoryType,
            amount: params.amount,
            description: params.description
            // amount: req.body.categoryType,
            // description: req.body.description
        });
        return user.save().then((user, err) =>{
            return err ? false : true;
        });
    });
};

//for user details 
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}


//Google login 

module.exports.verifyGoogleTokenId = async (tokenId) => {

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})

	console.log(data.payload.email_verified);

	if(data.payload.email_verified === true) {

		const user = await User.findOne({ email: data.payload.email });

		console.log(data.payload);

		if (user !== null) {
			
			if (user.loginType === "google") {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error : "login-type-error" }
			}

		} else {

			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
		        loginType: 'google'
			})

			return user.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})

		}

	} else {

		return { error: "google-auth-error" }

	}

};


// for delete

// module.export.archive = (params) =>{
//     return User.findById(params.userId).then((user) =>{
//         // 
//         console.log(user)
//     })
// }

// return User.findById(params.userId).then((user) =>{

//     module.exports.createCategory = (params) => {
//         console.log(params)
//         return User.findById(params.userId).then((user) =>{
//             user.categories.push({
//                 categoryName: params.categoryName,
//                 categoryType: params.categoryType
//             });
    
//             return user.save().then((user, err) =>{
//                 return err ? false : true;
//             });
//         });
//     };
    